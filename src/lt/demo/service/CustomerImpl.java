package lt.demo.service;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import lt.demo.date.DateFormat;



public class CustomerImpl {

	private Double amount;
	private Double interestRate;
	private Date date;
	private Double newInterestRate;
	private Date newDate;
	private String stringDate;
	private Integer term;
	private Double interestRateInDecimal;
	private Double power;
	private Double totalPayment;
	private Double interestPayment;
	private Double principalPayment;
	private Double remainingAmount;
	private Integer time = 1;

	public CustomerImpl(Double amount, Double interestRate, Date date, Integer term, Double newInterestRate, Date newDate) {

		this.amount = amount;
		this.interestRate = interestRate;
		this.date = date;
		this.term = term;
		this.amount = amount;
		this.newInterestRate = newInterestRate;
		this.newDate = newDate;

		try {
			print();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void print() throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File("Annuity payment schedule.csv"));
		StringBuilder sb = new StringBuilder();
		sb.append("Payment #"); 
		sb.append(',');
		sb.append("Payment date");
		sb.append(',');
		sb.append("Remaining amount");
		sb.append(',');
		sb.append("Principal payment");
		sb.append(',');
		sb.append("Interest payment");
		sb.append(',');
		sb.append("Total payment");
		sb.append(',');
		sb.append("Interest rate");
		sb.append('\n');

		data(sb);

		pw.write(sb.toString());
		pw.close();
		
	}

	private void data(StringBuilder sb) {
		for (int i = 1; i <= term; i++) {

			if (i == 1) {
				interestRate();
				power();
				totalPayment();
				calculate();
				addData(sb, i);

			} else {
				if (newDate.compareTo(date) <= 0 && time ==1) {
					interestRate();
					power();
					totalPayment();
					remainingAmount();
					calculate();
					interestRate = newInterestRate;
					addData(sb, i);
			        time++;

				} else {
					remainingAmount();
					calculate();
					addData(sb, i);
				}
			}
		}

	}
	
	public Date getDate() {
		return date;
	}

	public Date getNewDate() {
		return newDate;
	}

	private void addMonth() {
		date = DateUtils.addMonths(date, 1);

	}

	private void getStringDate(Date date) {
		stringDate = new DateFormat().getStringDate(date);

	}

	private void addData(StringBuilder sb, int i) {
		sb.append(i);
		sb.append(',');
		sb.append(stringDate);
		sb.append(',');
		sb.append(amount);
		sb.append(',');
		sb.append(principalPayment);
		sb.append(',');
		sb.append(interestPayment);
		sb.append(',');
		sb.append(totalPayment);
		sb.append(',');
		sb.append(interestRate);
		sb.append('\n');
	}

	private void calculate() {
		interestPayment();
		principalPayment();
		paymentDate();

	}

	private void paymentDate() {
		addMonth();
		getStringDate(date);

	}

	private void remainingAmount() {
		amount = amount - principalPayment;

	}

	private void principalPayment() {
		principalPayment = totalPayment - interestPayment;

	}

	private void interestPayment() {
		interestPayment = amount * interestRateInDecimal;
		interestPayment = (double) Math.round(interestPayment * 100) / 100;

	}

	private double decimalFormat(Double number) {
		return number = Math.floor(number * 100) / 100;

	}

	private void totalPayment() {
		totalPayment = amount * interestRateInDecimal / (1 - power);
		totalPayment = decimalFormat(totalPayment);
	}

	private void power() {
		power = Math.pow((1 + interestRateInDecimal), -term);
		System.out.println(power);

	}

	private void interestRate() {
		interestRateInDecimal = interestRate / (12 * 100);
		System.out.println(interestRateInDecimal);

	}

}


package lt.demo.controller;

import java.text.ParseException;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import lt.demo.date.DateFormat;
import lt.demo.entity.Customer;
import lt.demo.service.CustomerImpl;



@Controller
public class CustomerController {
	
	
      //transforming an empty string into a null value.
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {

		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);

	}

	@RequestMapping("/")
	public String showForm(Model model) {

		model.addAttribute("customer", new Customer());
		return "customer-form";

	}

	@RequestMapping("/processForm")
	public String processForm(@Valid @ModelAttribute("customer") Customer theCustomer, BindingResult theBindingResult)
			throws ParseException {
		

		Double amount = theCustomer.getAmount();
		Double interestRate = theCustomer.getInterestRate();
		String stringDate = theCustomer.getDateString();
		Integer term = theCustomer.getTerm();
		Double newInterestRate;
		String newStringDate;
		if (theCustomer.getNewInterestRate()!= null && theCustomer.getNewDateString()!= null ) {
			newInterestRate = theCustomer.getNewInterestRate();
			newStringDate = theCustomer.getNewDateString();
		} else {
			newInterestRate = interestRate;
			newStringDate = stringDate;
		}


		DateFormat dateformater = new DateFormat();
		Date date = dateformater.getDate(stringDate);
		Date newDate = dateformater.getDate(newStringDate);
		
		
		new CustomerImpl(amount, interestRate, date, term, newInterestRate, newDate);
		


		if (theBindingResult.hasErrors()) {
			return "customer-form";
		} else {
			return "customer-confirmation";

		}

	}

}


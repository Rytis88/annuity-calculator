package lt.demo.entity;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Customer {
	

	@NotNull(message="is required")
	@Min(value=100, message="Greater than 100 is required")
	@Max(value=1000000, message="Less than 1000000 is required")
	private Double amount; 
	
	@NotNull(message="is required")
	@Min(value=0, message="Greater than 0 is required")
	@Max(value=20, message="Less than 20 is required")
	private Double interestRate; 
	

	private Double newInterestRate; 
	
	@NotNull(message="is required")
	@Min(value=0, message="Greater than 0 is required")
	@Max(value=500, message="Less than 500 is required")
	private Integer term; 
	
	@NotNull(message="is required")
	private String dateString;
	

	private String newDateString;
		
	private Date date;
	
	public Double getNewInterestRate() {
		return newInterestRate;
	}

	public void setNewInterestRate(Double newInterestRate) {
		this.newInterestRate = newInterestRate;
	}

	public String getNewDateString() {
		return newDateString;
	}

	public void setNewDateString(String newDateString) {
		this.newDateString = newDateString;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}
	
	
}


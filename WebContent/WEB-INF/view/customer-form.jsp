<!-- Tagas form  -->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!Doctype html>

<html>
<head>
<title>Annuity Calculator</title>

<style>
    .error {color:red}
</style>
</head>

<body>

<h3>Monthly annuity calculator and the amortization schedule of the loan</h3>

<i>Fill out the form. Asterisk (*) means required.</i>

<br></br>

	<form:form action="processForm" modelAttribute="customer">

	Amount (*): <br><form:input path="amount"/>
	<form:errors path="amount" cssClass="error"/>
	<br></br>
	Interest rate (*): <br><form:input path="interestRate"/>
	<form:errors path="interestRate" cssClass="error"/>

<br></br>
Date (*): <br><form:input path="dateString" placeholder="mm/dd/yyyy"/>
<form:errors path="dateString" cssClass="error"/>
<br></br>

Term of loan (*): <br><form:input path="term" placeholder="months"/> 
<form:errors path="term" cssClass="error"/>
<br></br>


Date of new interest rate: <br><form:input path="newDateString" placeholder="mm/dd/yyyy"/>
<br></br>
New interest rate: <br><form:input path="newInterestRate"/>
<br></br>
<input type="submit" value="Submit"/>
</form:form>


</body>

</html>